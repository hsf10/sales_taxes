require_relative '../lib/input'
require_relative '../lib/transformer'
require_relative '../lib/operator'
require_relative '../lib/show'

class TaxRunner

    def initialize(filename)
        @filename = filename
    end

    def input(filename)
        file = Input.new(filename)
    end

    def parse(file, exclusions)
        list = Transformer.new(file, exclusions)
        list.generate
        return list
    end

    def calc(list)
        costs = Operator.new(list)
        costs.execute
        return costs
    end

    def print(items, sales_tax, total)
        show = Show.new(items, sales_tax, total)
        show.execute
        return show
    end

    def execute
        input = input(@filename)
        parsed_list = parse(input.items, input.exclusions)
        calculate = calc(parsed_list.items)
        print(calculate.items, calculate.sales_tax, calculate.total_all)
    end
end