require_relative './lib/input'
require_relative './lib/transformer'
require_relative './lib/operator'
require_relative './lib/show'
require_relative './lib/tax_runner'

filename = ARGV.first
purchase = TaxRunner.new(filename)
purchase.execute