### How to Run
`ruby launcher.rb <filename>.txt`
*Note*: The text file must be located in the `input` folder.
#### Input Files:
- input1.txt
- input2.txt
- input3.txt
***
### Testing
Tests were done with `rspec`.  
`rspec` - run test